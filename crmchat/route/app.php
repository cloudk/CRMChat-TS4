<?php

use think\facade\Route;

Route::get('admin/:path', function (string $path) {
    $filename = public_path(). 'admin/' . $path;
    var_dump($filename);
    return new \think\swoole\response\File($filename);
})->pattern(['path' => '.*\.\w+$']);


