<?php
// +----------------------------------------------------------------------
// | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2016~2020 https://www.crmeb.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
// +----------------------------------------------------------------------
// | Author: CRMEB Team <admin@crmeb.com>
// +----------------------------------------------------------------------

// 事件定义文件
return [
    'bind' => [

    ],

    'listen' => [
        'AppInit' => [],
        'HttpRun' => [],
        'HttpEnd' => [],
        'LogLevel' => [],
        'LogWrite' => [],
        'swoole.init' => [\crmeb\listeners\InitSwooleLockListen::class, \crmeb\listeners\SwooleStartListen::class], //swoole 初始化事件
        'swoole.shutDown' => [\crmeb\listeners\SwooleShutdownListen::class], //swoole 停止事件
        'swoole.websocket.user' => [\app\webscoket\handler\UserHandler::class], //socket 用户调用事件
        'swoole.websocket.admin' => [\app\webscoket\handler\AdminHandler::class], //socket 后台事件
        'swoole.websocket.kefu' => [\app\webscoket\handler\KefuHandler::class], //socket 客服事件
    ],

    'subscribe' => [
        \crmeb\subscribes\TaskSubscribe::class
    ],
];
